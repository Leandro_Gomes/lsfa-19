%!TEX root = ./tase.tex

The notion of an automaton \cite{Kleene56} as the \emph{de facto} mathematical abstraction of a computational process over discrete spaces, is being constantly revisited to capture different sorts of computational behaviour in the most varied contexts, either prescribed in a program or discovered in Nature. Already in 1997 Robin Milner \cite{Milner2006} emphasised that 
\vspace{0.3cm}
\begin{center}
\begin{minipage}{0.46\textwidth}
{\small
\emph{from being a prescription for how to do something -- in Turing's terms a `list of instructions', software
 becomes much more akin to  a description of behaviour, not only programmed on a
 computer,
but also occurring by hap or design inside or outside it.}}
\end{minipage}
\end{center}
\vspace{0.3cm}
Over time different kinds of automata have been proposed accordingly, \emph{generating} (or \emph{recognising}, depending on the perspective) such behaviours (or the languages that express them). In this context, Kleene algebra was introduced \cite{Kozen90} as an algebraic structure to capture axiomatically the basic properties of regular expressions. Several natural interpretations and variants are documented in the literature.


This paper focus on a specific sort of automata and languages, often arising in modelling natural phenomena, in which two extra ingredients cannot be overlooked. The first is \emph{uncertainty}. In a biological  network \cite{FMC17}, for example, this expresses the possibility of a certain enzime being absent or scarce in certain configurations. The other is \emph{simultaneity}, i.e. the fact that certain events (for example the flow of some reagents in a chemical reaction) are required to happen at the same time, instead of, as usually considered in interleaving models of concurrency, in a non deterministic alternation.

The first ingredient ---  \emph{uncertainty} --- is formalised by the notion of a \emph{fuzzy} finite-state automata (FFA), a structure introduced in the Sixties \cite{Wee1969} to give a formal semantics to uncertainty and vagueness inherent to several computational systems. Several variants of this idea, e.g. incorporating fuzziness to either  states or transitions, or both, are well documented in the literature \cite{fuzzyautomata}, \cite{LiP05}, \cite{MateescuSSY95}. In any case, \emph{fuzzy} languages \cite{LeeZ69,Zadeh1996} are recognised by a FFA only up to a certain membership degree. Applications are transversal to several domains \cite{LinY02}, \cite{Mordeson2002}, \cite{PedryczG01}, \cite{Ying02a}.

On its turn, \emph{simultaneity}, our second ingredient, was suitably formalised in what Milner called the `synchronous version of CCS' --- the SCCS \cite{Milner83} calculus, a variant of CCS \cite{Mil80} where arbitrary actions can run \emph{synchronously}. This very same idea of synchronous evolution appears in the work of C. Priscariu on synchronous Kleene algebra (SKA) \cite{Prisacariu10}. Kleene algebras are idempotent, and thus partially ordered, semirings endowed with a closure operator. Models for SKA, as well as for its variant with tests (SKAT), are given in terms of sets of synchronous strings and finite automata accepting synchronous strings. These structures found application, for instance, in variants of deontic logic to formalise contract languages \cite{Segerberg1982,vonWright1968-VONAEI-3}, and of Hoare logic to reason about parallel synchronous programs with shared variables \cite{Prisacariu10}.



                                                                                                                                                                                                                                            
\begin{figure}
\centering
\begin{tikzpicture}%[scale=0.2]
		%	baseline=(current bounding box.north)]
		[shorten >=1pt,node distance=2.2cm,on grid,auto]
		%\node[state,initial] (s_0) {$s_0$};
		\node[state,initial] [scale=1] (x_1) {$y_0$};
		\node[state] (x_2) [right=of x_1, right=2.5cm] [scale=1] {$y_1$};
		\node[state] (x_3) [below=of x_1, below=2.5] [scale=1] {$y_2$};
		\node[state,accepting](x_4) [below=of x_2,below=2.5] [scale=1] {$y_3$};
		\path[->]
		%(x_1) edge node [scale=0.35] [above] [sloped] {($\{coffee,milk\}$,$\delta^{coffee\times milk}((x_1,x_3),\{coffee,milk\},(x_2,x_4))$)} (x_4)
		(x_1) edge node [scale=0.8] [above] [sloped] { $m$} (x_2)
		(x_1) edge node [scale=0.8] [above] [left] { $c$} (x_3)
		(x_2) edge node [scale=0.8] [above] [left] { $c$} (x_4)
		(x_3) edge node [scale=0.8] [above] {$m$} (x_4);	
		\end{tikzpicture}
	\caption{Interleaving two automata representing deterministic flows.}
	\label{fig:inter}
\end{figure}

Both of these ingredients are combined in this paper. The sort of systems we have in mind is illustrated in Fig.~\ref{fig:inter}. Suppose that two automata represent the flow of two reagents $c$ and $m$ into a solution. The scheme of Fig.~\ref{fig:inter} represents their interleaving as two alternative sequential compositions. Our objective, however, has a different focus. First we intend to record that elementary steps are `uncertain', in the sense that each individual flow may exhibit failures or interruptions. The transitions in the automata are thus labelled with the flow identifier and a `certainty' degree measuring how certain it is for each flow (\emph{event}) effectively flowing. Second, their combination makes sure that both flows (\emph{actions}) occur simultaneously combining their `certainty' degrees into the `certainty' degree of their joint flow. The reader is invited to have a quick look at automata depicted by the end of section III, which express these features. As we will make clear in the sequel, such are the \emph{fuzzy synchronous} automata this paper proposes to add to the broad family of finite-state automata mentioned above.

%\begin{figure}[h]
%	\begin{minipage}{\textwidth}
%		\begin{tikzpicture}[shorten >=1pt,node distance=1cm,on grid,auto]
%		\node[state,initial] (x_0) {$x_0$}; 
%		\node[state,accepting] (x_1) [right=of x_1,%right=1cm, label={[align=left]coffee}
%		] {$x_1$};
%		\path[->]
%		(x_0) edge node {coffee} (x_1);
%		\end{tikzpicture}
%	\end{minipage}
%\end{figure}	
%\noindent and
%
%	\begin{figure}[h]
%	\begin{minipage}{\textwidth}
%		\begin{tikzpicture}
%		[shorten >=1pt,node distance=1cm,on grid,auto]
%				\node[state,initial] (x_1) [%right=of x_0,right=1cm
%			%	label={[align=left]coffee}
%				] {$x_1$};
%				\node[state,accepting] (x_2) [right of =x_1,right=2cm,% label={[align=left]coffee \text{and }milk\\(cappuccino)}
%				] {$x_2$};
%				\path[->]
%				(x_1) edge node {milk} (x_2);
%		\end{tikzpicture}
%	\end{minipage}
%\end{figure}

In order to formalise such a behaviour, the paper introduces a synchronous  product construction between a variant of fuzzy transition automata in the spirit of reference \cite{MateescuSSY95}, where transitions take `certainty' values in a complete Heyting algebra $\SH$. We designate such automata as $\SH$-NFA.  
The notion of synchronous sets in reference \cite{Prisacariu10} is generalised to that of \emph{fuzzy synchronous languages}, and some operators over them suitably defined. A map that interprets the terms of SKA as fuzzy synchronous languages is  provided. Then we prove that the terms of a SKA %let us say the regular expressions formed by such terms,
can be used to generate a $\SH$-NFA  accepting precisely the fuzzy synchronous language that constitutes its interpretation. Obtaining a regular expression from a $\SH$-NFA proceeds by state elimination as in the classical case  \cite{ullman03}. The  procedure results in a $\SH$-NFA with a single transition from the initial to the final state, labelled by an action $\alpha$ of SKA such that its interpretation is the language recognised by that $\SH$-NFA. %$\widehat{FI}_{SKA}(\alpha)=\mathcal{L(M_\alpha)}$. Finally, we show that any subset of fuzzy synchronous languages with a signature consisting of the defined operators over them is a SKA.
 
This paper is organised as follows. The remaining of this section recaps some fundamental concepts required later. Section
\ref{sec:model} introduces fuzzy synchronous languages and defines dome suitabe operators over them. Section \ref{sec:automata} introduces a method for constructing the synchronous product of two nondeterministic automata with fuzzy transitions. Moreover, it is proved that the algebra constituted by any set of fuzzy synchronous languages and the signature previously defined forms a SKA. Finally, Section \ref{sec:conc} concludes and enumerates some topics for future research.

%\subsection{Approximate string matching with using automata}

\subsection{Kleene algebra}

\begin{defn}[Kleene algebra]\label{def:ka}
	A \emph{Kleene algebra}  $(K,+,\cdot,^*,\0,\un)$ is an idempotent semiring with an extra unary operator $^*$ satisfying the axioms $(\ref{eqn1})-(\ref{eqn13})$ of Fig.~\ref{fig:axska}. A partial order $\leq$ is defined as $\alpha\leq\beta\Leftrightarrow \alpha+\beta=\beta$.
\end{defn}
\medskip

The operators $+$, $\cdot$ and $*$ are typically understood as \emph{nondeterministic choice}, \emph{sequence} and \emph{iteration}, respectively. Actions $\0$ and $\un$ represent \emph{fail} and  \emph{skip}, respectively.

Well-known examples of Kleene algebras are the algebra of binary relations over a set $X$, the set of all languages over $\Sigma^*$, and the $(min,+)$ algebra, also know as the tropical algebra.

\subsection{Synchronous Kleene algebra}
Extending the original definition with an operation $\times$ to capture the synchronous execution of actions\footnote{Following \cite{Prisacariu10}, the symbol $\times$ stands for the synchronous product; any possible confusion with the same symbol used for Cartesian product is desambiguated by context.} lead to the notion of 
a \emph{synchronous Kleene algebra} \cite{Prisacariu10}.%Now, beyond of the Kleene algebra structure, it is consider an operation $\times$ for the synchronous composition of actions, the actions sets $\mathcal{A}$ are structured by $\mathcal{A}_B\subseteq \mathcal{A}_B^{\times}\subset \mathcal{A}$, where $\mathcal{A}_B$ is just a set of basic symbols, and $\mathcal{A}_B^{\times}\subset \mathcal{A}$ is its closure under $\times$.
 %. The elements of $A_B^{\times}$ are called $\times$-actions and are denoted as $\alpha_{\times}$. Summing up, $A_B\subseteq A_B^{\times}\subset A$.

\begin{defn}[Synchronous Kleene algebra]
		 A synchronous Kleene algebra (SKA) is a tuple $$\mathbf{S}=(\mathcal{A},+,\cdot,\times,^*,\mathbf{0},\mathbf{1},\mathcal{A}_B)$$
		  satisfying the axioms in Fig.~\ref{fig:axska}.
		 
		 \begin{figure}[ht]
		 	%\begin{minipage}{0.50\linewidth}
		 		\begin{eqnarray}
		 		 %& &\text{All axioms of Kleene algebra}\nonumber\\
		 		 \alpha+ (\beta +\gamma) & = & (\alpha+ \beta) +\gamma \label{eqn1}\\
		 		 			\alpha +\beta & = & \beta + \alpha \label{eqn2}\\
		 		 			\alpha +\alpha & = & \alpha \label{eqn3}\\
		 		 			\alpha + 0 & = &  0 + \alpha  = \alpha\label{eqn4}\\
		 		 			\alpha\cdot(\beta\cdot\gamma) & = & (\alpha\cdot\beta)\cdot\gamma \label{eqn5}\\
		 		 			\alpha\cdot\mathbf{1} & = & \un\cdot\alpha = \alpha \label{eqn6}\\
		 		 			\alpha\cdot(\beta +\gamma ) & =& (\alpha\cdot\beta) + (\alpha\cdot\gamma) \label{eqn7}\\
		 		 			(\alpha +\beta)\cdot\gamma  & =& (\alpha\cdot\gamma) + (\beta\cdot\gamma)  \;\;\label{eqn8}\\
		 		 			\alpha\cdot\0 & = & \0\cdot\alpha = \0 \label{eqn9}\\
		 		 			\un+(\alpha\cdot\alpha^*)& = & \alpha^* \label{eqn10}\\
		 		 		%	\end{eqnarray}
		 		 	%	\end{minipage}
		 		 	%	\quad
		 		 	%	\begin{minipage}{0.50\linewidth}
		 		 	%		\begin{eqnarray}
		 		 			\un+(\alpha^*\cdot\alpha)& = & \alpha^* \label{eqn11}\\
		 		 			\beta+\alpha\cdot\gamma\leq\gamma & \Rightarrow & \alpha^*\cdot\beta\leq \gamma\label{eqn12}\\
		 		 			\beta+\gamma\cdot\alpha\leq\gamma & \Rightarrow & \beta\cdot\alpha^*\leq\gamma\label{eqn13}\\
		 		 			%\alpha\cdot\beta \leq x & \Rightarrow &  \alpha^*\cdot\beta \leq x \label{eqn12} \\
		 		 			%\beta\cdot\alpha \leq x & \Rightarrow &  \beta\cdot\alpha^* \leq x \label{eqn13}\\
		 		\alpha\times (\beta \times \gamma) & = & (\alpha\times \beta) \times \gamma \label{eqn30}\\
		 		\alpha \times \beta & = & \beta \times \alpha \label{eqn31}\\
		 		\alpha\times \mathbf{1} & = & \mathbf{1}\times \alpha = \alpha \label{eqn32}\\
		 		\alpha \times \mathbf{0} & = &  \mathbf{0} \times \alpha  = \0 \label{eqn33}\\
		 		a_b \times a_b & = & a_b, a_b\in \mathcal{A}_B \label{eqn34}\\		 		
		 		\alpha\times(\beta +\gamma ) & =& (\alpha\times \beta) + (\alpha\times \gamma) \label{eqn35}\\
		 		(\alpha +\beta)\times \gamma & =& (\alpha\times \gamma) + (\beta\times \gamma)  \;\;\label{eqn36}\\
		 		(\alpha_{\times}\cdot \alpha)\times (\beta_{\times}\cdot \beta)&=&(\alpha_{\times}\times \beta_{\times})\cdot(\alpha\times \beta), \alpha_{\times},\beta_{\times}\in \mathcal{A}_B^{\times}
		 		 \label{eqn37}
		 		\end{eqnarray}
		 %	\end{minipage}
		 \caption{Axiomatisation of SKA from \cite{Prisacariu10}.}
		 \label{fig:axska}
		 \end{figure}
		 % $A_B$ is the set of basic actions, $A_B^{\times}\subset A$ is the set $A_B$ closed under $\times$. The elements of $A_B^{\times}$ are called $\times$-actions and are denoted as $\alpha_{\times}$. Summing up, $A_B\subseteq A_B^{\times}\subset A$.
		 % Note, particularly, for axiom \ref{eqn37}, where $ \alpha_{\times},\beta_{\times}\in \mathcal{A}_B^{\times}$.
		 
		\end{defn}
		 
		 The sets of actions $\mathcal{A}$ and $\mathcal{A}_B$ are structured by $\mathcal{A}_B\subseteq \mathcal{A}_B^{\times}\subset \mathcal{A}$, where $\mathcal{A}_B$ is the set of basic actions and $\mathcal{A}_B^{\times}$ is its closure under $\times$.
		 
		 We denote by $T_{SKA}$ the term algebra of SKA, generated by the grammar:
		 
		 $$\alpha::= a_b\mid\mathbf{0}\mid\mathbf{1}\mid\alpha+\alpha\mid\alpha\cdot\alpha\mid\alpha\times\alpha\mid\alpha^*$$
		 
		 \noindent where $a_b\in A_B$. Following a common practice, we write $a_b b_b$, rather than $a_b\cdot b_b$, for $a_b$, $b_b$ $\in A_B$. The elements of $\mathcal{A}_B^{\times}$ are called \emph{$\times$-actions} (e.g. $a$, $a\times b\in \mathcal{A}_B^{\times}$ but $a+b$, $a\times b+c$, $\0$, $\un\notin \mathcal{A}_B^{\times}$).
		 
%\subsection{Action lattice}
%
%\begin{defn}[Action lattice]\label{def:actionlattice}
%	An \emph{action lattice} is a tuple $$\A=(A,+,;,*,0,{}1,\rightarrow,\cdot)$$
%	\noindent where $(A,+,;,*,0,1)$ is a Kleene algebra $\rightarrow$ and $\cdot$ are binary operations in $A$
%	satisfying the axioms enumerated in Figure~\ref{fig:axal}, where the relation $\leq$ is defined as in Definition \ref{def:ka}.
%	An \emph{$\mathbb{I}$ action lattice} consists of an action lattice satisfying $1= \top$.
%	A \emph{$\mathbb{H}$-action lattice} is an $\mathbb{I}$-action lattice where $;=.$, i.e. for all $a,b\in A$, $a;b=a.b$.
%	
%\end{defn}
%	
%	\begin{figure}[ht]
	%	\begin{minipage}{0.50\linewidth}
%			\begin{eqnarray}
%			a+ (b +c) & = & (a+ b) +c \label{eqn1}\\
%			a +b & = & b + a \label{eqn2}\\
%			a +a & = &  a \label{eqn3}\\
%			a + 0 & = &  0 + a  = a\label{eqn4}\\
%			a;(b;c) & = & (a;b);c \label{eqn5}\\
%			a;1 & = & 1;a = a \label{eqn6}\\
%			a;(b +c ) & =& (a;b) + (a;c) \label{eqn7}\\
%			(a +b);c  & =& (a;c) + (b;c)  \;\;\label{eqn8}\\
%			a;0 & = & 0;a = 0 \label{eqn9}\\
%			1+(a;a^*)& = & a^* \label{eqn10}\\
		%	\end{eqnarray}
	%	\end{minipage}
	%	\quad
	%	\begin{minipage}{0.50\linewidth}
	%		\begin{eqnarray}
%			1+(a^*;a)& = & a^* \label{eqn11}\\
%			a;x \leq x & \Rightarrow &  a^*;x \leq x \label{eqn12} \\
%			x;a \leq x & \Rightarrow &  x;a^* \leq x \label{eqn13}\\
%			a;x \leq b & \Leftrightarrow & x \leq a \rightarrow b \label{eqn14} \\
			%a\rightarrow b &\leq& a\rightarrow (b+c)\label{eqn23}\\
			%x;a \leq b & \Leftrightarrow & x \leq a \leftarrow b \label{eqn14}\\ 
			%(x\rightarrow x)^* & = & x\rightarrow x \label{eqn15}\\ 
			%x&\leq& a\rightarrow (a;x) \label{eqn24}\\
			%(x\leftarrow x)^* & = & x \leftarrow x \label{eqn16}\\ 
%			a \cdot ( b \cdot c) & = & (a \cdot  b) \cdot c \label{eqn17}\\
%			a \cdot  b & = &  b \cdot a \label{eqn18}\\
%			a \cdot  a & = &  a \label{eqn19}\\
%			a + ( a \cdot b) & = & a \label{eqn20}\\
%			a \cdot (a+b) & = & a \label{eqn21}
			% a & \leq & 1 \label{eq:integral}\\
			% a;b & = & b;a  \label{eq:commutative}
%			\end{eqnarray}
	%	\end{minipage}
%		\caption{Axiomatisation of action lattices (from \cite{onactionalgebras})}
%		\label{fig:axal}
%	\end{figure}
	
	\subsection{Complete Heyting algebra}
	
	% \begin{defn}[Complete Heyting algebra]
	% 	A complete Heyting algebra is a structure
	% 	$\He=(\SH,+,;,\0,\un)$		
	% 	which can be also seen as a complete distributive lattice where $+$ is the supremum and $;$ is the infimum, and has a partial order $\leq$ defined in the usual way as $h_1\leq h_2\Leftrightarrow h_1+h_2=h_2$, for $h_1,h_2\in \SH$. Since $+$ is associative, we can generalise it to a $n$-ary operator and use the notation $\bigvee$ to represent their iterated version. Moreover, $\He$ is a quantale, hence satisfying the infinite distributive laws
	% 	$ h;(\bigvee_{i\in I}h_i)=\bigvee_{i\in I}(h;h_i)$ and
	% 	$(\bigvee_{i\in I}h_i);h=\bigvee_{i\in I}(h_i;h)$
	% 	 for arbitrary $h,h_i\in \SH$ and $i\in I$, with $I$ being an index set. The operator $;$ acts as both infimum and multiplication; $\0$ its the least element and $\un$ is the identity element for $;$ operator. Note that, since the multiplication and the infimum coincide, $\un$ is the greatest element of $\He$. Consequently, $\He$ is a quatale that is unital, integral, idempotent and comutative. Summarising, $\He$ satisfies the axioms (\ref{eqn1})-(\ref{eqn9}) of Figure \ref{fig:axska}, replacing $\cdot$ by $;$ and the following properties of infimum:
	% 	\begin{figure}[h]
	% 		\begin{eqnarray}
	% 			h_1;h_2&=&h_2;h_1\label{eqn18}\\
	% 			h;h&=&h\label{eqn19} 
	% 		\end{eqnarray}
	% 	\end{figure}
	% \end{defn}

%\begin{defn}[Quantale]
%	A \emph{quantale} is a complete lattice with a multiplication operator $;$ satisfying the infinite distributive laws $h;(\bigvee_{i\in I}h_i)=\bigvee_{i\in I}(h;h_i)$ and $(\bigvee_{i\in I}h_i);h=\bigvee_{i\in I}(h_i;h)$ for arbitrary $h,h_i\in H$ and $i\in I$, with $I$ being an index set. 
%	A \emph{quantale} $\SH=(H,+,;,\0,\un)$ is a complete distributive lattice where $+$ is the supremum and $;$ is the infimum, and has a partial order $\leq$ defined in the usual way as $h_1\leq h_2\Leftrightarrow h_1+h_2=h_2$, for $h_1,h_2\in H$. Since $+$ is associative, we can generalise it to a $n$-ary operator and use the notation $\bigvee$ to represent their iterated version. Hence, $\SH$ satisfies the infinite distributive laws
%	$ h;(\bigvee_{i\in I}h_i)=\bigvee_{i\in I}(h;h_i)$ and
%	$(\bigvee_{i\in I}h_i);h=\bigvee_{i\in I}(h_i;h)$
%	for arbitrary $h,h_i\in H$ and $i\in I$, with $I$ being an index set. The operator $;$ acts as both infimum and multiplication; $\0$ its the least element and $\un$ is the identity element for $;$ operator.
%\end{defn}

%If a quantale has an identity element $\un$ for multiplication, it is called a \emph{unital quantale}.
%If the multiplication is commutative, it is called \emph{commutative quantale}.

	\begin{defn}[Complete Heyting algebra]
		A complete Heyting algebra (CHA) is a tuple
		$$\SH=(H,+,;,\0,\un,\to)$$	
		% which can be also seen as a complete distributive lattice where $+$ is the supremum and $;$ is the infimum, and has a partial order $\leq$ defined in the usual way as $h_1\leq h_2\Leftrightarrow h_1+h_2=h_2$, for $h_1,h_2\in \SH$. Since $+$ is associative, we can generalise it to a $n$-ary operator and use the notation $\bigvee$ to represent their iterated version. Moreover, $\He$ is a quantale, hence satisfying the infinite distributive laws
		% $ h;(\bigvee_{i\in I}h_i)=\bigvee_{i\in I}(h;h_i)$ and
		% $(\bigvee_{i\in I}h_i);h=\bigvee_{i\in I}(h_i;h)$
		%  for arbitrary $h,h_i\in \SH$ and $i\in I$, with $I$ being an index set. The operator $;$ acts as both infimum and multiplication; $\0$ its the least element and $\un$ is the identity element for $;$ operator. Note that, since the multiplication and the infimum coincide, $\un$ is the greatest element of $\He$. Consequently, $\He$ is a quatale that is unital, integral, idempotent and comutative. Summarising, $\He$ satisfies 
		which satisfies axioms (\ref{eqn1})-(\ref{eqn9}) in Fig.~\ref{fig:axska}, replacing $\cdot$ by $;$ and, additionally, the following axioms:
			\begin{eqnarray}
				h_1;h_2&=&h_2;h_1\label{eqn18}\\
				h;h&=&h\label{eqn19}\\
				h_1+(h_1;h_2)&=&h_1\label{eqn20}\\
				h_1;h_2\leq h_3 & \Leftrightarrow & h_2\leq h_1\to h_3\label{eqn21}\\
				h;(\sum_{i\in I}h_i)&=&\sum_{i\in I}(h;h_i)\label{eqn22}\\
		 		(\sum_{i\in I}h_i);h&=&\sum_{i\in I}(h_i;h)\label{eqn23}
			\end{eqnarray}
			with $\Sigma$ denotating the iterated version of the associative operator $+$, and $I$ being an (possible infinite) index set. 
	\end{defn}
	
	%A partial order $\leq$ is also defined in the usual way as $l_1\leq l_2\Leftrightarrow l_1+l_2=l_2$, for $l_1,l_2\in L$.
%
%	Note that $\SH=(H,+,;,\0,\un)$ can also be seen as a complete distributive lattice where $+$ is the supremum and $;$ is the infimum, and has a partial order $\leq$ defined in the usual way as $h_1\leq h_2\Leftrightarrow h_1+h_2=h_2$, for $h_1,h_2\in H$.
% Moreover, $\SH$ is a quantale, hence satisfying the infinite distributive laws
	%	 $ h;(\sum{i\in I}h_i)=\sum_{i\in I}(h;h_i)$ and
	%	 $(\sum{i\in I}h_i);h=\sum{i\in I}(h_i;h)$
	%	  for arbitrary $h,h_i\in H$ and $i\in I$, with $I$ being an index set. %The operator $;$ acts as both infimum and multiplication; $\0$ its the least element and $\un$ is the identity element for $;$ operator.% Note that, since the multiplication and the infimum coincide, $\un$ is the greatest element of $\SH$. %Consequently, $\SH$ is a quatale that is unital, integral, idempotent and comutative.
This ensures that all suprema exist when characterising operators $\cdot$, $\times$ and $^*$ on fuzzy synchronous languages as (possible) infinite sums. Moreover axiom (\ref{eqn21}) ensures that every suprema distributes over arbitrary infima, which is used to prove Theorem \ref{th:fuzzySKA}.
	The examples below illustrate this structure.
		
	\begin{exmp}[$\mathbf{2}$- the Boolean algebra]\label{2}
		%	(\textbf{2} - the Boolean lattice).
		A first example is the well-known binary structure
		$$\mathbf{2} = (\{\top, \bot\},\vee,\wedge,\bot,\top,\to)$$
		with the standard interpretation of Boolean connectives. %Operator $^*$ maps each element of $\{\top,\bot\}$ to $\top$.
	\end{exmp}
	
	\begin{exmp}\label{3}
		\noindent A second example is the three-element linear lattice, which introduces an explicit denotation $u$ for ``unknown'' (or ``undefined'').
		$\mathbf{3} = (\{\top, u, \bot\},\vee,\wedge,\bot,\top,\to)$
		where
		\begin{table}[H]
		\centering
			%\fontsize{10pt}{10pt}
			\selectfont
			%\centering
			\begin{tabular}{l|ccccc}
				$\vee$ & $\bot$ & u & $\top$ &  \\
				\hline
				$\bot$ & $\bot$ & $u$ & $\top$ &  \\
				$u$ & $u$ & $u$ & $\top$ &  \\
				$\top$ & $\top$ & $\top$ & $\top$ &
			\end{tabular} \hspace{0.2 cm}
			\begin{tabular}{l|ccccc}
				$\wedge$ & $\bot$ & $u$ & $\top$ &  \\
				\hline
				$\bot$ & $\bot$ & $\bot$ & $\bot$ &  \\
				$u$ & $\bot$ & $u$ & $u$ &  \\
				$\top$ & $\bot$ & $u$ & $\top$ &
			\end{tabular} \hspace{0.2 cm}
			\begin{tabular}{l|ccccc}
			$\to$ & $\bot$ & u & $\top$ & \\
			\hline
			$\bot$ & $\top$ & $\top$ & $\top$ & \\
			$u$ & $\bot$ & $\top$ & $\top$ & \\
			$\top$ & $\bot$ & $u$ & $\top$ &
		\end{tabular} \hspace{0.5 cm}
		\end{table}
	\end{exmp}
	
	%\begin{exmp}[Lukasiewicz]\label{luka}
	%	\noindent This example is based on the well-known \L ukasiewicz arithmetic algebra.
	%	\newline
	%	\[\boldsymbol{\L}=([0,1],max,\odot,^*,0,1)\]
	%	\newline
	%	\noindent where $x\odot y=max\{0,x+y-1\}$ and $^*$ maps each point of the interval $[0,1]$ to $1$.
	%\end{exmp}
	%
	%\begin{exmp}[$\Pi$(product)-algebra]\label{product}
	%	As another example, consider now the standard $\Pi$-algebra
	%	\newline
	%	\[\boldsymbol{\Pi}=([0,1],max,.,^*,0,1)\]
	%	\newline
	%	\noindent where $.$ is the usual multiplication of real numbers and $^*$ maps each point of the interval $[0,1]$ to $1$.
	%\end{exmp}
	
	\begin{exmp}[Gödel algebra]\label{godel}
		Another example is given by the Gödel algebra
		$\mathbf{G} = ([0,1],max,min,0,1,\to)$
	where \begin{equation*}
	x\to y=
	\begin{cases}
	1,\ \text{if}\ x\leq y\\
	y,\ \text{if}\ y< x
	\end{cases}
	\end{equation*}
	\end{exmp}
	
%	\begin{exmp}[\L ukasiewicz]\label{luka}
%		\noindent Another example is based on the well-known \L ukasiewicz arithmetic lattice.
%		\newline
%		\[\boldsymbol{\L}=([0,1],max,\odot,0,1)\]
%		\newline
%		\noindent where $x\odot y=max\{0,x+y-1\}$.
%	\end{exmp}
%	
%	\begin{exmp}\label{tropical}
%		The $(min, +)$ algebra \cite{DAA}, known as the tropical semiring constitutes another instance. First, let $R_+$ denote the set $\{x\in \mathbb{R}\mid x\geq 0\}$ and adjoin $\infty$ as a new constant. Thus, define
%		\newline
%		\[\boldsymbol{R}=(R_+\cup\{\infty\},min,+,\infty,0)\]
%	\end{exmp}
%	