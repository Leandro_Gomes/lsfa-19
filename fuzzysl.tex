%!TEX root = ./tase.tex

This section introduces a notion of \emph{fuzzy synchronous language}, based on C. Prisacariu proposal for the crisp synchronous case \cite{Prisacariu10}. A number of operations over fuzzy synchronous languages are also defined. Finally, a map that interprets each term of $T_{SKA}$ as a fuzzy synchronous language is formalised. % but now considering a set of fuzzy synchronous languages and operators over them instead of taking the set of synchronous languages.
The reader is referred to any classical introduction to fuzzy logic, e.g. \cite{Zadeh1996}, for the standard definitions of fuzzy sets and fuzzy languages used in the sequel.

%We construct our model on top of an arbitrary complete Heyting algebra, which can vary depending on the computational setting we want to describe.
%
% \begin{definition}[Fuzzy synchronous language]\label{def:fuzzy sunchronous language}
% 	Consider a finite set denoted $\mathcal{A}_B$, standing for the basic actions. We can build an alphabet $\Sigma=P(\mathcal{A}_B)\setminus\{\emptyset\}$, representing all nonempty subsets of $\mathcal{A}_B$ (denoted by $x,y$). From this alphabet we build \emph{synchronous strings} over $\Sigma$ %which are fuzzy subsets of $\Sigma^*$
% 	including the empty string $\epsilon$ (denoted by $u,v\in \Sigma^*$). A \emph{fuzzy synchronous language} is a fuzzy subset of synchronous strings from $\Sigma^*$ (denoted by $\mathcal{L}_1$, $\mathcal{L}_2$, $\mathcal{L}_3$). Given a CHA $\He$ with support set $\SH$, we denote the set of all fuzzy synchronous languages by $\SH^{\Sigma^*}$. Let $S\subseteq \SH^{\Sigma^*}$ be a set of fuzzy synchronous languages.	
% 	For given fuzzy synchronous languages $\mathcal{L}$, $\mathcal{L}_1$, $\mathcal{L}_2\in S$ and for all $w\in\Sigma^*$, we give the following definitions and operations over fuzzy synchronous languages:
	
% 	\begin{itemize}
% 		\item [-] $\varnothing(w)=0$, for all $w\in\Sigma^*$
% 		\item [-]
% 		$\chi(w)=
% 				\begin{cases}
% 				\un & \text{ if } w=\epsilon\\
% 				\0 & \text{otherwise}
% 				\end{cases}
% 				$
% 		\item [-] $(\mathcal{L}_1\cup\mathcal{L}_2)(w)$=$\mathcal{L}_1(w)+\mathcal{L}_2(w)$
% 		\item [-] $(\mathcal{L}_1\cdot\mathcal{L}_2)(w)=\bigvee_{u,v}\mathcal{L}_1(u);\mathcal{L}_2(v)$, \text{ with } $w=uv$ \text{ being the concatenation of strings } $u$ \text{ and } $v$
% 		\item [-] $\mathcal{L}^*(w)=\bigvee_{i\geq 0}\mathcal{L}_1^i(w)$, \text{ with } $\mathcal{L}^0=\mathbf{1}(w)$, $\mathcal{L}^{(i+1)}(w)=(\mathcal{L}.\mathcal{L}^i)(w)$
% 		\item [-] $(\mathcal{L}_1\times\mathcal{L}_2)(w)=\bigvee_{u,v}\mathcal{L}_1(u);\mathcal{L}_2(v)$, \text{with } $w=u\times v$ \text{defined as:}\\
% 		$u\times \epsilon=u=\epsilon\times u$\\
% 		$u\times v=(x\cup y)(u'\times v')$ \text{ where } $u=xu'$ \text{ and } $v=yv'$, \text{ with } $x,y\in \Sigma$
% 	\end{itemize}
	

\begin{defn}[$\SH$-Fuzzy synchronous language]\label{def:fuzzy synchronous language}
	Let $\mathcal{A}_B$ be a set of basic actions and $\SH$ a CHA over carrier $H$, and   $\Sigma=\mathcal{P}(\mathcal{A}_B)\setminus\{\emptyset\}$ the alphabet of all the nonempty subsets of $\mathcal{A}_B$ (denoted by $x,y$). 
	Sequences $u,v,\ldots\in \Sigma^*$ are called \emph{$\mathcal{A}_B$-synchronous strings}, with notation $\epsilon$
	standing for the empty string. A $\SH$-fuzzy synchronous language over $\mathcal{A}_B$ is an element of $\SH^{\Sigma^*}$, i.e. a function $\mathcal{L}: \Sigma^*\rightarrow H$.
\end{defn}

We can then generalise for this setting, the standard operators from regular language theory. For any $\SH$-fuzzy synchronous languages $\mathcal{L}$, $\mathcal{L}_1$, $\mathcal{L}_2$, and for all $w\in\Sigma^*$, we define the following operations:
\begin{itemize}
	\item [-] $\varnothing(w)=0$, for all $w\in\Sigma^*$
	\item [-]
	$\chi(w)=
	\begin{cases}
	\un & \text{ if } w=\epsilon\\
	\0 & \text{otherwise}
	\end{cases}
	$
	\item [-] $(\mathcal{L}_1\cup\mathcal{L}_2)(w)$=$\mathcal{L}_1(w)+\mathcal{L}_2(w)$
	\item [-] $(\mathcal{L}_1\cdot\mathcal{L}_2)(w)=\sum_{u,v}\mathcal{L}_1(u);\mathcal{L}_2(v)$, \text{ with } $w=uv$ \text{ standing for the concatenation of strings } $u$ \text{ and } $v$
	\item [-] $\mathcal{L}^*(w)=\sum_{i\geq 0}\mathcal{L}_1^i(w)$, \text{ with } $\mathcal{L}^0(w)=\chi(w)$, $\mathcal{L}^{(i+1)}(w)=(\mathcal{L}\cdot \mathcal{L}^i)(w)$
	\item [-] $(\mathcal{L}_1\times\mathcal{L}_2)(w)=\sum_{u,v}\mathcal{L}_1(u);\mathcal{L}_2(v)$, \text{with } $w=u\times v$ \text{defined by}
	\begin{itemize}
	\item $u\times \epsilon=u=\epsilon\times u$
	\item $u\times v=(x\cup y)(u'\times v')$ \text{ where } $u=xu'$ \text{ and } $v=yv'$, \text{ with } $x,y\in \Sigma$.
\end{itemize}
\end{itemize}
One may notice that the expressions that define operators $\cdot$ and $\times$ seem related. Note, however, that operator $\cdot$ ranges over all possible ways to construct the word $w$ by concatenation of the smaller words $u$ and $v$, while operator $\times$ looks over all the possible constructions by ``classical'' synchronous product of words $u\times v$ defined above.

\begin{defn}[Basic $\SH$-fuzzy and $\times-\SH$-fuzzy synchronous languages]
	A \emph{basic $\SH$-fuzzy synchronous language}, denoted by $\mathcal{L}_B$, is a $\SH$-fuzzy synchronous language such that $\mathcal{L}_B(w)=\0$ whenever $w\not \in \mathcal{A}_B$. A \emph{$\times-\SH$-fuzzy synchronous language}, denoted by $\mathcal{L}^{\times}$, is a $\SH$-fuzzy synchronous language such that $\mathcal{L}^\times(w)=\0$ whenever $w\not \in \mathcal{A}^\times_B$.
\end{defn}

Note that $\mathcal{A}_B^\times$ does not contain any action built from operator $\cdot$ (e.g. for $\mathcal{A}_B=\{a,b,c\}$, $abc\notin \mathcal{A}_B^\times$).

% %	\begin{equation}\label{eq:basic actions}
% %	\mathcal{L}_B(w)=	
% %	\begin{cases}
% %		\alpha\in $A$ & \text{ if } w=a, \text{ with } a\in \mathcal{A}_B\\
% %		0, & \text{otherwise}
% %	\end{cases}
% %	\end{equation}
% 	\begin{definition}[$\times$-fuzzy synchronous language]
% 		A $\times$-fuzzy synchronous language is defined only for elements of $\mathcal{A}_B^\times$ and is denoted by ${}^h\mathcal{L}^{\times}$, for $h\in\SH$. Note that $\mathcal{A}_B^\times$ does not contain any action built with the operator $\cdot$ (e.g. for $\mathcal{A}_B=\{a,b,c\}$, $abc\notin \mathcal{A}_B^\times$). This way, 
% 	${}^h\mathcal{L}^{\times}$ is defined similarly as $\mathcal{L}_B^h$, for $h\in\SH$. For any $x\in \Sigma$ and for $h\in\SH$,

% 	\[
% 	{}^h\mathcal{L}^\times(w)=
% 	\begin{cases}
% 	h & \text{ if } w=x\\
% 	\0 & \text{otherwise}
% 	\end{cases}
% 	\]

% 	\noindent for any $w\in \Sigma^*$.
% \end{definition}
% 	%Note that actions built from elements of $\mathcal{A}_B$ using operator $\cdot$ (e.g. $abc$) are not in $\mathcal{A}_B^\times$ and so $\mathcal{L}^{\times}$ is defined the same way as $\mathcal{L}_B^x$, for $x\in\SH$.



\noindent  Without loss of generality, we write $a_b$ for the singleton set $\{a_b\}$, for any $a_b\in\mathcal{A}_B$. Moreover, expression $a_1\ldots a_n$, for $n\geq 1$ will denote in the sequel a synchronous string %$\{x_1\}\ldots\{x_n\}$
where $a_i\in \Sigma$, with $1\leq i\leq n$. 

%\vspace{-0.5cm}

Similarly to the homomorphism used to interpret SKA as synchronous sets \cite{Prisacariu10}, we define a map to interpret term actions of $\alpha\in T_{SKA}$ as $\SH$-fuzzy synchronous languages. 
% Given a CHA $\mathbf{H}$ with support $\mathbf{H}$, let us denote $\mathcal{FASS}$ a subalgebra of $H^{\Sigma^*}$ which contains $\varnothing$, $\chi$ and all $\mathcal{L}_B^h$, for $h\in\SH$ and for all $a_b\in \mathcal{A}_B$, where $\mathcal{A}_B$ is finite and defined beforehand.

\begin{defn}[Fuzzy interpretation]\label{def:fi}
	%An interpretation of SKA is a homomorphism with domain the term algebra $T_{SKA}$.
	Consider a map $FI_{SKA}:\mathcal{A}_B\cup\{\mathbf{0},\mathbf{1}\}\to H^{\Sigma^*}$ such that
	
	\begin{itemize}
		\item $FI_{SKA}(a_b)=\mathcal{L}_B$ %defined as (\ref{eq:basic actions})
		\item  $FI_{SKA}(\mathbf{0})=\varnothing$
		\item  $FI_{SKA}(\mathbf{1})=\chi$
	\end{itemize}where $\mathcal{L}_B$ is a basic $\SH$-fuzzy synchronous language such that $\mathcal{L}_B(w)=\0$ for all $w\neq a_b$.
	
	\noindent Its extension $\widehat{FI}_{SKA}:T_{SKA}\to H^{\Sigma^*}$ over the term algebra is called a \emph{fuzzy interpretation} of $SKA$ and  defined as
	
	\begin{itemize}
		\item [] $\widehat{FI}_{SKA}(\alpha)=FI_{SKA}(\alpha), \forall \alpha\in \mathcal{A}_B\cup\{\mathbf{0},\mathbf{1}\}$
		\item [] $\widehat{FI}_{SKA}(\alpha+\beta)=\widehat{FI}_{SKA}(\alpha)\cup\widehat{FI}_{SKA}(\beta)$
		\item [] $\widehat{FI}_{SKA}(\alpha\cdot\beta)=\widehat{FI}_{SKA}(\alpha)\cdot \widehat{FI}_{SKA}(\beta)$
		\item [] $\widehat{FI}_{SKA}(\alpha\times\beta)=\widehat{FI}_{SKA}(\alpha)\times\widehat{FI}_{SKA}(\beta)$
		\item [] $\widehat{FI}_{SKA}(\alpha^*)=\widehat{FI}_{SKA}(\alpha)^*$
	\end{itemize}
	
	%Note however that, contrary to the interpretation of synchronous sets \cite{Prisacariu10}, the map here defined is not a homomorphism.
	
\end{defn}
